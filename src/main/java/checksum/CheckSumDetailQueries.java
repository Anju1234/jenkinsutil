package checksum;

/**
 * Created by anjukumari on 18/10/18
 */
public abstract class CheckSumDetailQueries {
    public static String getPrefId_query = "select id from merchant_preference_info where merchant_id='{?}' and pref_type='CHECKSUM_ENABLED';";
    public static String getAlipayMerchantIdQuery = "SELECT id from alipay_paytm_merchant where paytm_merchant_id='{?}'";
    public static String updateChecksumQuery = "UPDATE merchant_preference_info SET pref_value='{?}' where id={?} and merchant_id='{?}'";

    CheckSumDetailQueries(){

    }


    public abstract void execute(String mid);

}
