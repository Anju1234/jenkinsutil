package checksum;

import Implementation.ExecutionConfigs;
import Util.CommonUtil;
import Util.DatabaseUtil;
import Util.redisUtil;

/**
 * Created by anjukumari on 18/10/18
 */
public class DisableChecksum extends CheckSumDetailQueries {

    DisableChecksum(){

    }

    public void execute(String mid) {
        try {
            String sql_query = CommonUtil.getUpdatedStaring(CheckSumDetailQueries.getAlipayMerchantIdQuery, mid);
            System.out.println("Query to get merchant id from alipay_merchant_id table :" + sql_query);
            String merchantId = null;
            try {
                merchantId = DatabaseUtil.getInstance().executeSelectQuery(Implementation.ExecutionConfigs.PGP_DB_URL, sql_query).get(0).get("id").toString();
            } catch (IndexOutOfBoundsException e) {
                System.out.println("merchant not found in DB ");
                return;
            }
            sql_query = CommonUtil.getUpdatedStaring(CheckSumDetailQueries.getPrefId_query, merchantId);
            System.out.println("Query to get prefernece id from merchant preference info table :" + sql_query);
            String perfId = DatabaseUtil.getInstance().executeSelectQuery(Implementation.ExecutionConfigs.PGP_DB_URL, sql_query).get(0).get("id").toString();
            sql_query = CommonUtil.getUpdatedStaring(CheckSumDetailQueries.updateChecksumQuery, "N", perfId, merchantId);
            System.out.println("Query to Update merchant checksum :" + sql_query);
            DatabaseUtil.getInstance().executeUpdateQuery(ExecutionConfigs.PGP_DB_URL, sql_query);
            redisUtil.getInstance().getConnection(ExecutionConfigs.PGP_REDIS_SERVER).flushAll();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public static void main(String[] args){
        String mid = args[0];
        System.out.println("mid for checksum is "+mid);
        new DisableChecksum().execute(mid);
    }
}
