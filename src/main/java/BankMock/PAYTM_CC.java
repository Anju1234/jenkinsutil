package BankMock;

import Implementation.ExecutionConfigs;
import Util.DatabaseUtil;

/**
 * Created by anjukumari on 09/07/18
 */
public class PAYTM_CC extends BankDetailsQueries{
    String channels;
    String payMethods;

    PAYTM_CC(){
        channels = "WAP,WEB";
        payMethods = "PAYTM_DIGITAL_CREDIT";
        bankName = "PAYTMCC";
        payMethodList = payMethods.split(",");
        channelList = channels.split(",");
    }

    //mock urls
    String Mock_WEB_PAY_URL = "https://pgp-automation.paytm.in/mockbank/digitalcredit";
    //String Mock_STATUS_QRY_URL = "https://pgp-automation.paytm.in/mockbank/digitalcredit/v2/user/checkBalance";
    String Mock_STATUS_QRY_URL = "https://pgp-automation.paytm.in/mockbank/digitalcredit/transact/status";
    String Mock_S2S_PAY_URL = "https://pgp-automation.paytm.in/mockbank/digitalcredit";
    //original urls
    String Orig_WEB_PAY_URL = " https://fs-staging.paytm.com/digitalcredit";
    //String Orig_STATUS_QRY_URL = "https://fs-staging.paytm.com/digitalcredit/transact/status";
    String Orig_STATUS_QRY_URL = "https://fs-staging.paytm.com/digitalcredit/transact/status";
    String Orig_S2S_PAY_URL = "https://fs-staging.paytm.com/digitalcredit";

    @Override
    public void execute(String operation){

        for (int i = 0; i < payMethodList.length; i++) {
            for (int j = 0; j < channelList.length; j++) {
                setBankDetails(channelList[j], bankName, payMethodList[i]);
                sql_query = getUpdatedStaring(sql_query, bankId, payMethodId, channelId);
                String WEB_PAY_URL = null;
                String STATUS_QRY_URL = null;
                String S2S_PAY_URL = null;
                System.out.println(" Details in DB before update for "+channelList[j]+ "  ,and  "+payMethodList[i]+" is : >>>> \n"+ DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, sql_query).toString());

                if(operation.equalsIgnoreCase("apply")){
                    WEB_PAY_URL = Mock_WEB_PAY_URL;
                    STATUS_QRY_URL = Mock_STATUS_QRY_URL;
                    S2S_PAY_URL = Mock_S2S_PAY_URL;
                }else if(operation.equalsIgnoreCase("remove")){
                    WEB_PAY_URL = Orig_WEB_PAY_URL;
                    STATUS_QRY_URL = Orig_STATUS_QRY_URL;
                    S2S_PAY_URL = Orig_S2S_PAY_URL;
                }
                String changeWebPayUrl = getUpdatedStaring(sql_update_query, "WEB_PAY_URL",WEB_PAY_URL,bankId, payMethodId, channelId );
                System.out.println("query to update web pay url is:  "+changeWebPayUrl);
                String changeStatusQueryUrl = getUpdatedStaring(sql_update_query, "STATUS_QRY_URL",STATUS_QRY_URL, bankId, payMethodId, channelId);
                System.out.println(" query to update status query url is:  "+changeStatusQueryUrl);
                String changeS2SPayUrl = getUpdatedStaring(sql_update_query, "S2S_PAY_URL",S2S_PAY_URL, bankId, payMethodId, channelId);
                System.out.println(" query to update s2s pay url is:  "+changeS2SPayUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeWebPayUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeStatusQueryUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeS2SPayUrl);
            }
        }
        Util.redisUtil.getInstance().flush(ExecutionConfigs.PGP_REDIS_SERVER);

    }

    public static  void main(String[] args){
        PAYTM_CC object = new PAYTM_CC();
        object.execute(args[0]);
    }



}
