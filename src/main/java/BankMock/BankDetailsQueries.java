package BankMock;

import Implementation.ExecutionConfigs;
import Util.DatabaseUtil;

/**
 * Created by anjukumari on 20/06/18
 */
public abstract  class BankDetailsQueries {
    String  bankName;
    String[]  payMethodList;
    String[] channelList;
    String bankId;
    String channelId;
    String payMethodId;

    String sql_update_query;
    String sql_query;
    String channelId_query;
    String bankId_query;
    String payMethodId_query;


    BankDetailsQueries(){

        sql_update_query="update BANK_URL_INFO set {?}='{?}'"
                +" where BANK_ID='{?}' " + " and PAY_METHOD_ID='{?}' and CHANNEL_ID='{?}'";

        sql_query="select BANK_ID,CHANNEL_ID,PAY_METHOD_ID,WEB_PAY_URL,WEB_RESPONSE_URL,REFUND_URL " +
                "from BANK_URL_INFO where" + " BANK_ID='{?}' and PAY_METHOD_ID='{?}' and CHANNEL_ID='{?}'";

        channelId_query = "select ID from LOOKUP_DATA where NAME = '{?}'";
        bankId_query = "select BANK_ID from BANK_MASTER where BANK_CODE ='{?}'";
        payMethodId_query = "select ID from LOOKUP_DATA where NAME ='{?}'";

    }

    public static String getUpdatedStaring(String ...args){
        for(int i=1;i<=args.length-1;i++){
            args[0]  = args[0].replaceFirst("\\{\\?\\}",args[i]);
        }
        return args[0];

    }


    public void setBankDetails(String channel, String bank, String payMethod) {

                payMethodId = DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, getUpdatedStaring(payMethodId_query, payMethod)).get(0).get("ID").toString();
                channelId = DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, getUpdatedStaring(channelId_query, channel)).get(0).get("ID").toString();
                bankId = DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, getUpdatedStaring(bankId_query, bankName)).get(0).get("BANK_ID").toString();

    }

    public abstract void execute(String operation);




}
