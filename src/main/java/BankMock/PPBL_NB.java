package BankMock;

import Implementation.ExecutionConfigs;
import Util.DatabaseUtil;

/**
 * Created by anjukumari on 14/09/18
 */
public class PPBL_NB extends BankDetailsQueries{
    String channels;
    String payMethods;

    PPBL_NB(){
        channels = "WAP,WEB";
        payMethods = "NB";
        bankName = "PPBL";
        payMethodList = payMethods.split(",");
        channelList = channels.split(",");
    }

    //mock urls
    String Mock_S2S_PAY_URL = "https://pgp-automation.paytm.in/mockbank/transaction/ext/v1/funds-transfer";
    //original urls
    String Orig_S2S_PAY_URL = "https://secure-ite2.paytmbank.com/transaction/ext/v1/funds-transfer";


    @Override
    public void execute(String operation) {
        for (int i = 0; i < payMethodList.length; i++) {
            for (int j = 0; j < channelList.length; j++) {
                setBankDetails(channelList[j], bankName, payMethodList[i]);
                sql_query = getUpdatedStaring(sql_query, bankId, payMethodId, channelId);
                String S2S_PAY_URL = null;
                System.out.println(" Details in DB before update for "+channelList[j]+ "  ,and   "+payMethodList[i]+": >>>>\n"+ DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, sql_query).toString());
                if(operation.equalsIgnoreCase("apply")){
                    S2S_PAY_URL = Mock_S2S_PAY_URL;
                }else if(operation.equalsIgnoreCase("remove")){
                    S2S_PAY_URL = Orig_S2S_PAY_URL;
                }
                String changeS2SPayUrl = getUpdatedStaring(sql_update_query, "S2S_PAY_URL",S2S_PAY_URL, bankId, payMethodId, channelId);
                System.out.println(" query to update s2s pay url is:  "+changeS2SPayUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeS2SPayUrl);
            }
        }
        Util.redisUtil.getInstance().flush(ExecutionConfigs.PGP_REDIS_SERVER);
    }

    public static  void main(String[] args){
        PPBL_NB object = new PPBL_NB();
        object.execute(args[0]);
    }
}
