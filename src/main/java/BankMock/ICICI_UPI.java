package BankMock;

import Implementation.ExecutionConfigs;
import Util.DatabaseUtil;

/**
 * Created by anjukumari on 20/06/18
 */
public class ICICI_UPI extends BankDetailsQueries {
    String channels;
    String payMethods;

    ICICI_UPI() {
        bankName = "ICICI";
        payMethods = "UPI";
        channels = "WAP,WEB";
        payMethodList = payMethods.split(",");
        channelList = channels.split(",");
    }

    String Mock_WEB_PAY_URL = "https://pgp-automation.paytm.in/mockbank/api/MerchantAPI/UPI/v2/CollectPay";
    String Mock_STATUS_QRY_URL = "https://pgp-automation.paytm.in/mockbank/api/MerchantAPI/UPI/v2/TransactionStatus";
    String Mock_REFUND_URL = "https://pgp-automation.paytm.in/mockbank/api/MerchantAPI/UPI/v1/Refund/125004";
    String Orig_WEB_PAY_URL = "https://api.icicibank.com:8443/api/MerchantAPI/UPI/v2/CollectPay";
    String Orig_STATUS_QRY_URL = "https://api.icicibank.com:8443/api/MerchantAPI/UPI/v2/TransactionStatus";
    String Orig_REFUND_URL = "https://apigwuat.icicibank.com:8443/api/MerchantAPI/UPI/v1/Refund/125004";

    @Override
    public void execute(String operation) {
        for (int i = 0; i < payMethodList.length; i++) {
            for (int j = 0; j < channelList.length; j++) {
                setBankDetails(channelList[j], bankName, payMethodList[i]);
                sql_query = getUpdatedStaring(sql_query, bankId, payMethodId, channelId);
                String WEB_PAY_URL = null;
                String STATUS_QRY_URL = null;
                String REFUND_URL = null;
                System.out.println(" before changes for " + channelList[j] + "," + payMethodList[i] + ": " + DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, sql_query).toString());

                if (operation.equalsIgnoreCase("apply")) {
                    WEB_PAY_URL = Mock_WEB_PAY_URL;
                    STATUS_QRY_URL = Mock_STATUS_QRY_URL;
                    REFUND_URL = Mock_REFUND_URL;
                } else if (operation.equalsIgnoreCase("remove")) {
                    WEB_PAY_URL = Orig_WEB_PAY_URL;
                    STATUS_QRY_URL = Orig_STATUS_QRY_URL;
                    REFUND_URL = Orig_REFUND_URL;
                }
                String changeWebPayUrl = getUpdatedStaring(sql_update_query, "WEB_PAY_URL", WEB_PAY_URL, bankId, payMethodId, channelId);
                String changeStatusQueryUrl = getUpdatedStaring(sql_update_query, "STATUS_QRY_URL", STATUS_QRY_URL, bankId, payMethodId, channelId);
                String changeRefundUrl = getUpdatedStaring(sql_update_query, "REFUND_URL", REFUND_URL, bankId, payMethodId, channelId);
                System.out.println(">>>" + changeWebPayUrl + "::::" + changeStatusQueryUrl + "::::" + changeRefundUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeWebPayUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeStatusQueryUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeRefundUrl);

            }
        }
        Util.redisUtil.getInstance().flush(ExecutionConfigs.PGP_REDIS_SERVER);
    }

    public static void main(String[] args) {
        ICICI_UPI object = new ICICI_UPI();
        object.execute(args[0]);
    }


}
