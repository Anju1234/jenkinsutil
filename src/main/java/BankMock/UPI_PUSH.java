package BankMock;

import Implementation.ExecutionConfigs;
import Util.DatabaseUtil;

/**
 * Created by anjukumari on 09/10/18
 */
public class UPI_PUSH extends BankDetailsQueries{
    String channels;
    String payMethods;

    UPI_PUSH(){
        channels = "WAP,WEB";
        payMethods = "UPI";
        bankName = "PPBEX";
        payMethodList = payMethods.split(",");
        channelList = channels.split(",");
    }

    String Mock_WEB_PAY_URL = "https://pgp-automation.paytm.in/mockbank/bankFormatter/iciciFormatter/request1.jsp";
    String Mock_STATUS_QRY_URL = "https://pgp-automation.paytm.in/mockbank/corp/BANKAWAY?IWQRYTASKOBJNAME=bay_mc_login&BAY_BANKID=ICI";
    String Orig_WEB_PAY_URL = "https://shopping.icicibank.com/corp/BANKAWAY?IWQRYTASKOBJNAME=bay_mc_login&BAY_BANKID=ICI";
    String Orig_STATUS_QRY_URL = "https://shopping.icicibank.com/corp/BANKAWAY?IWQRYTASKOBJNAME=bay_mc_login&BAY_BANKID=ICI";
    @Override
    public void execute(String operation){

        for (int i = 0; i < payMethodList.length; i++) {
            for (int j = 0; j < channelList.length; j++) {
                setBankDetails(channelList[j], bankName, payMethodList[i]);
                sql_query = getUpdatedStaring(sql_query, bankId, payMethodId, channelId);
                String WEB_PAY_URL = null;
                String STATUS_QRY_URL = null;
                System.out.println(" before changes for>>>>>>> "+channelList[j]+ "  ,  "+payMethodList[i]+": >>>>"+ DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, sql_query).toString());

                if(operation.equalsIgnoreCase("apply")){
                    WEB_PAY_URL = Mock_WEB_PAY_URL;
                    STATUS_QRY_URL = Mock_STATUS_QRY_URL;
                }else if(operation.equalsIgnoreCase("remove")){
                    WEB_PAY_URL = Orig_WEB_PAY_URL;
                    STATUS_QRY_URL = Orig_STATUS_QRY_URL;
                }
                String changeWebPayUrl = getUpdatedStaring(sql_update_query, "WEB_PAY_URL",WEB_PAY_URL,bankId, payMethodId, channelId );
                System.out.println("web pay url query:  "+changeWebPayUrl);
                String changeStatusQueryUrl = getUpdatedStaring(sql_update_query, "STATUS_QRY_URL",STATUS_QRY_URL, bankId, payMethodId, channelId);
                System.out.println(" status query:  "+changeStatusQueryUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeWebPayUrl);
                Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, changeStatusQueryUrl);

            }
        }
        Util.redisUtil.getInstance().flush(ExecutionConfigs.PGP_REDIS_SERVER);

    }

    public static  void main(String[] args){
        ICICI_NB object = new ICICI_NB();
        object.execute(args[0]);
    }


}
