package Implementation;

import Util.DatabaseUtil;
import Util.PropertyUtil;

import java.sql.*;
import java.util.List;
import java.util.Map;

/**
 * Created by anjukumari on 19/06/18
 */
public class MockDBChanges {
    static String  bankName;
    static String[]  payMethodList;
    static String payMethod;
    static String[] channelList;
    static String channel;
    static String bankId;
    static String channelId;
    static  String payMethodId;


    static{
        PropertyUtil.getInstance().load("urlConfig.properties");
        bankName = PropertyUtil.getInstance().getPropertyValue("BANKNAME");
        payMethodList = PropertyUtil.getInstance().getPropertyValue("PAY_METHOD").split(",");
        channelList = PropertyUtil.getInstance().getPropertyValue("CHANNEL_ID").split(",");

    }

    public static String getUpdatedStaring(String ...args){
        for(int i=1;i<=args.length-1;i++){
            args[0]  = args[0].replaceFirst("\\{\\?\\}",args[i]);
        }
        return args[0];

    }



   /* private  static String channelId_query = "select ID from LOOKUP_DATA where NAME = '{channel}'";//+channel+"'";
    private  static String bankId_query = "select BANK_ID from BANK_MASTER where BANK_CODE ='{bankName}'";//+bankName+"'";
    private static String payMethodId_query = "select ID from LOOKUP_DATA where NAME ='{payMethod}'";//+Implementation.MockDBChanges.payMethod+"'";
*/

    private  static String channelId_query = "select ID from LOOKUP_DATA where NAME = '{?}'";//+channel+"'";
    private  static String bankId_query = "select BANK_ID from BANK_MASTER where BANK_CODE ='{?}'";//+bankName+"'";
    private static String payMethodId_query = "select ID from LOOKUP_DATA where NAME ='{?}'";//+Implementation.MockDBChanges.payMethod+"'";

   /* sql_update_query="update BANK_URL_INFO set WEB_RESPONSE_URL=" +
            "'"+Implementation.ExecutionConfigs.ENV_BASE_URL+"/instaproxy/bankresponse/"+bankName+"/CC' " +
            "where BANK_ID='"+bankId+"'" + "and PAY_METHOD_ID='"+payMethodId+"' and CHANNEL_ID='"+channelId+"'";

    sql_query="select BANK_ID,CHANNEL_ID,PAY_METHOD_ID,WEB_PAY_URL,WEB_RESPONSE_URL " +
            "from BANK_URL_INFO where" + " BANK_ID='"+bankId+"' and PAY_METHOD_ID='"+payMethodId+"' and CHANNEL_ID='"+channelId+"'";
*/


    static String sql_update_query="update BANK_URL_INFO set WEB_RESPONSE_URL=" +
            "'{?}/instaproxy/bankresponse/{?}/CC' " +
            "where BANK_ID='{?}'" + "and PAY_METHOD_ID='{?}' and CHANNEL_ID='{?}'";

    static String sql_query="select BANK_ID,CHANNEL_ID,PAY_METHOD_ID,WEB_PAY_URL,WEB_RESPONSE_URL " +
            "from BANK_URL_INFO where" + " BANK_ID='{?}' and PAY_METHOD_ID='{?}' and CHANNEL_ID='{?}'";


    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        List<Map<String,Object>> resultList;
        for(int i =0 ;i<payMethodList.length;i++){
            payMethod = payMethodList[i];
            payMethodId = DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, getUpdatedStaring(payMethodId_query, payMethod)).get(0).get("ID").toString();
            for(int j = 0; j< channelList.length; j++) {
                channel = channelList[j];
                channelId = DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, getUpdatedStaring(channelId_query,channel)).get(0).get("ID").toString();
                bankId = DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, getUpdatedStaring(bankId_query,bankName)).get(0).get("BANK_ID").toString();

                sql_update_query = getUpdatedStaring(sql_update_query, ExecutionConfigs.ENV_BASE_URL, bankName, bankId, payMethodId);
                sql_query = getUpdatedStaring(sql_query, bankId, payMethodId, channelId);

                System.out.println(" before changes for "+channel+ ","+payMethod+": "+ DatabaseUtil.getInstance().executeSelectQuery(ExecutionConfigs.PGP_DB_URL, sql_query).toString());
                // Util.DatabaseUtil.getInstance().executeUpdateQuery(Implementation.ExecutionConfigs.PGP_DB_URL, sql_update_query);
                //System.out.println(" after changes: "+Util.DatabaseUtil.getInstance().executeSelectQuery(Implementation.ExecutionConfigs.PGP_DB_URL, sql_query).toString());
            }
        }
    }


}
