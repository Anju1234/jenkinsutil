package Implementation;

import Util.PropertyUtil;

/**
 * Created by anjukumari on 20/06/18
 */
public class ExecutionConfigs {
    public static String PGP_DB_URL;
    public static String AUTH_DB_CONNECTION_URL;
    public static String WALLET_DB_CONNECTION_URL;
    public static String ENV_BASE_URL;
    public static String PGP_REDIS_SERVER;
    public static String INSTA;



    static{
        PropertyUtil.getInstance().load("urlConfig.properties");
        PGP_DB_URL = PropertyUtil.getInstance().getPropertyValue("PGP_DB_CONNECTION_URL");
        AUTH_DB_CONNECTION_URL = PropertyUtil.getInstance().getPropertyValue("AUTH_DB_CONNECTION_URL");
        ENV_BASE_URL = PropertyUtil.getInstance().getPropertyValue("ENV_BASE_URL");
        PGP_REDIS_SERVER = PropertyUtil.getInstance().getPropertyValue("PGP_REDIS_SERVER");
        INSTA = PropertyUtil.getInstance().getPropertyValue("INSTA=10.144.18.102");
    }





}
