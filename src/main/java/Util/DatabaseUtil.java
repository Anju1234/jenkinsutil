package Util;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;

import java.sql.*;
import java.util.List;
import java.util.Map;

/**
 * Created by anjukumari on 20/06/18
 */
public class DatabaseUtil {
    static DatabaseUtil dbConnection;
    static private Connection connection;
    static private Statement statement;

    public  static synchronized DatabaseUtil getInstance(){
        if(dbConnection == null){
            dbConnection = new DatabaseUtil();
        }
        return dbConnection;
    }

    public  synchronized Connection getConnection(String dbConnectionUrl) {
        try {
            String[] arr = dbConnectionUrl.split(":");
            String dbDriver = arr[1];
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            connection  = DriverManager.getConnection(dbConnectionUrl);

        }catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException();
        }
        return connection;
    }

    public synchronized  void closeConnection(String dbConnectionUrl){
        try {
            if (!connection.isClosed()) {
                connection.close();
            }
        }catch (SQLException e){
            throw new RuntimeException();
        }
    }

    public synchronized List<Map<String, Object>> executeSelectQuery(String dbConnectionUrl, String sqlQuery){
        try {
            ResultSet resultSet;
            connection = getConnection(dbConnectionUrl);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sqlQuery);
            MapListHandler mapListHandler = new MapListHandler();
            List<Map<String, Object>>  resultList = mapListHandler.handle(resultSet);
            return resultList;
        }catch (SQLException e){
            throw new RuntimeException();
        }
        finally {
            DbUtils.closeQuietly(statement);
        }
    }


    public synchronized void executeUpdateQuery(String dbConnectionUrl, String sqlQuery){
        try {
            connection = getConnection(dbConnectionUrl);
            statement = connection.createStatement();
            statement.executeUpdate(sqlQuery);
        }catch (SQLException e){
            throw  new RuntimeException();
        }finally {
            DbUtils.closeQuietly(statement);
        }
    }

}
