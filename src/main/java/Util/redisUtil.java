package Util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

/**
 * Created by anjukumari on 26/06/18
 */
public class redisUtil {

    public static redisUtil redisConnection;
    public static Jedis redis;
    public static synchronized redisUtil getInstance(){
        if(redisConnection == null){
            redisConnection = new redisUtil();
        }else{

        }
            return redisConnection;
    }

    private JedisPoolConfig getJedisPoolConfig() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(500);
        poolConfig.setMaxIdle(100);
        poolConfig.setMinIdle(10);
        poolConfig.setMaxWaitMillis(1000);
        poolConfig.setFairness(true);
        poolConfig.setBlockWhenExhausted(false);
        poolConfig.setTestOnCreate(true);
        return poolConfig;
    }

    public  Jedis getConnection(String redisServerUrl){


        redis = new JedisPool(getJedisPoolConfig(), redisServerUrl.split(":")[0], Integer.valueOf(redisServerUrl.split(":")[1]), Protocol.DEFAULT_TIMEOUT, null,
                0).getResource();
        return redis;
    }

    public void flush(String serverIp){
        redis = getConnection(serverIp);
        redis.flushAll();
    }


    public static void close(String serverIp){
        redis.close();
    }


}
