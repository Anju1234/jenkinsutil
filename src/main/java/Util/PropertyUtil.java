package Util;

import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by anjukumari on 19/06/18
 */
public class PropertyUtil {

    public static PropertyUtil prop;
    private Properties property = new Properties();

    public static synchronized PropertyUtil getInstance() {
        if (prop == null) {
            prop = new PropertyUtil();
        }

        return prop;
    }



    @Test
    public Properties load(String fileName){
        try{

            InputStream input = PropertyUtil.class.getClassLoader().getResourceAsStream(fileName);
            this.property.load(input);
            System.out.println("::"+property.getProperty("PGP_DB_CONNECTION_URL"));
        }catch(IOException e){
            e.printStackTrace();
        }
        return property;
    }

    public String getPropertyValue(String key){
        return property.getProperty(key);
    }





}
